### inChurch Recruitment Process 2020 - iOS Developer

Primeiramente, gostaria de pedir desculpas, pois eu não havia visto que deveria ter sido realizado um Fork deste projeto, e acabei criando um repo no GitHub, onde ficaram armazenados todos os commits do [projeto](https://github.com/Matelaa/inchurch-ios-challenge).

### Arquitetura do Projeto
- Arquitetura utilizada: MVVM
- Esta arquitetura foi utilizada pois possibilita uma abstração de informações para a ViewController, enviando para ela, apenas as informações que serão apresentadas na tela já prontas para serem exibidas. A View não se comunica diretamente com as Models, esta comunicação é feita somente através da ViewModel.
    
    - Models: Modelos do que estará armazenado no banco de dados.
    - ViewModels: Classes responsáveis por acessar o banco de dados, coletar as informações necessárias, tratá-las e enviá-las para as Controllers prontas para serem exibidas.
    - View: Responsável por definir a estrutura que o usuário vê na tela.

### Bibliotecas Utilizadas
Neste projeto foram utilizadas as seguintes bibliotecas:

- Alamofire;
    - Usada para realizar as requisições a API do [MovieDB](https://www.themoviedb.org/?language=pt-BR).
- ObjectMapper;
    - Usada para faciliar a conversão dos objetos das Model's para JSON e vice-versa.
- AlamofireObjectMapper;
    - Usada para realizar a conversão das respostas das requisições em objetos para se usar no ObjectMApper
- SwiftGen;
    - Usada para gerar alguns arquivos que facilitem o seu uso, evitar erros e coisas "hardcoded".
- RealmSwift;
    - Banco de dados utilizado no projeto.
- Reusable;
    - Usada para não ter problemas/erros ao registrar algumas celulas/controllers assim evitando coisas "hardcoded".
- Kingfisher;
    - Usada para conseguir usar imagens no projeto.
- SwiftMessages;
    - Usada para dar ao usuario os feedbacks de erro no projeto.
- StatefulViewController;
    - Utilizada para exibir os placeholders referentes ao estado da controller.
<hr>

### Requisitos

- O que dever ter
    - Tela de Home com duas abas:
        - Filmes: Um grid exibindo os filmes melhores classificados. Utilizar esse [endpoint](https://developers.themoviedb.org/3/movies/get-popular-movies). :white_check_mark:
        - Favoritos: Uma listagem dos filmes marcados como favorito. :white_check_mark:
    - Loading no carregamento da listagem de filmes.
    - Tela de tratamento de erros (falta de internet e erro na api) na tela de Filmes. :white_check_mark:
    - Tela de detalhe do filme. Para as informações de gêneros do filme, utilize esse [endpoint](https://developers.themoviedb.org/3/genres/get-movie-list). :white_check_mark:
        - Observação: Não foi feito a parte de pegar a informação de gêneros do filme.
    - Ação de favoritar um filme na tela de detalhe. Todo o controle será em armazenamento local. :white_check_mark:

- Pontos Extras
    - Paginação com scroll infinito na tela de filmes. :x:
    - Filtro de busca pelo nome do filme na tela de Favoritos. Exibir uma tela diferente para quando não houver resultado na busca. :x:
    - Ação de remover o filme da lista de Favoritos. :white_check_mark:
    - Testes unitários. :x:
    - Testes funcionais. :x: